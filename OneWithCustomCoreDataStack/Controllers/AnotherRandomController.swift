//
//  AnotherRandomController.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 9/30/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

class AnotherRandomController {

    //MARK: - Private Api
    private var taskOnMainContext: TaskMOC?
    private lazy var coreDataController = CoreDataController()

    
    func deleteTask(task: TaskMOC?) {
        if let taskMoc = task, let moc = task?.managedObjectContext {
            self.coreDataController.deleteTaskItem(taskMoc, onContext: moc)
        }
    }
}

