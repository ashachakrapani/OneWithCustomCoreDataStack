//
//  SomeRandomController.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 9/11/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

class SomeRandomController {
    
    init () {
        NotificationCenter.default.addObserver(self, selector: #selector(self.managedContextObjectContextDidSave(notification:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: CoreDataStack.sharedInstance.privateManagedObjectContext)
        self.createChildContext()
    }
    
    //MARK: - Private Api
    private var taskOnMainContext: TaskMOC?
    private var taskOnRandomChildContext: TaskMOC?
    private var randomChildContext: NSManagedObjectContext?
    
    @objc private func managedContextObjectContextDidSave(notification: NSNotification) {
        print("Merging changes")
        self.randomChildContext?.performAndWait {
            self.randomChildContext?.mergeChanges(fromContextDidSave: notification as Notification)
            if let _taskOnRandomChildContext = self.taskOnRandomChildContext {
                self.randomChildContext?.refresh(_taskOnRandomChildContext, mergeChanges: true)
                print("Random Controller ------------------------")
                self.printSelectedTaskOnAllContexts()
            }
        }
    }
    
    private func createChildContext() {
        self.randomChildContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        self.randomChildContext?.parent = CoreDataStack.sharedInstance.mainViewContext
    }
    
    func getTaskOnRandomChildContext(task: TaskMOC?) {
        self.randomChildContext?.performAndWait {
            self.taskOnMainContext = task
            if let _taskOnMainContext = self.taskOnMainContext {
                do {
                    self.taskOnRandomChildContext = try randomChildContext?.existingObject(with: _taskOnMainContext.objectID) as? TaskMOC
                } catch {
                    print("Error obtaining existingObjectById \(error)")
                }
            }
        }
    }
    
    func printSelectedTaskOnAllContexts() {
        CoreDataStack.sharedInstance.mainViewContext.performAndWait {
            if let _taskOnMain = self.taskOnMainContext {
                print ("Task on Main View Context \(String(describing: _taskOnMain.notes?.notes))")
            }
        }
        self.randomChildContext?.performAndWait {
            if let _taskOnRandomChild = self.taskOnRandomChildContext {
                print ("Task on Random Child View Context \(String(describing: _taskOnRandomChild.notes?.notes))\n")
            }
        }
    }
}
