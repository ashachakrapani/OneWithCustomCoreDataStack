//
//  EditViewController.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 8/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

class EditController {
    
    
    //MARK: - Private Api
    private var taskOnChildContext: TaskMOC?
    private var taskOnMainContext: TaskMOC?
    private var childContext: NSManagedObjectContext?
    
    
    //MARK: - Public Api
    func edit(task: TaskMOC?) {
        //modify task1 on child context and save child context
        self.createChildContext()
        self.childContext?.performAndWait {
            self.taskOnMainContext = task
            if let _taskOnMainContext = self.taskOnMainContext, let _childContext = self.childContext {
                do {
                    self.taskOnChildContext = try childContext?.existingObject(with: _taskOnMainContext.objectID) as? TaskMOC
                    self.taskOnChildContext?.notes = NotesMOC.insertInto(context: _childContext, notes: "Updated notes for task")
                } catch {
                    print("Error obtaining existingObjectById \(error)")
                }
            }
        }
    }
    
    func createChildContext() {
        self.childContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        self.childContext?.parent = CoreDataStack.sharedInstance.mainViewContext
    }
    
    func printSelectedTaskOnAllContexts() {
        print("Edit Controller -------------------")
        CoreDataStack.sharedInstance.mainViewContext.performAndWait {
            print ("Task notes on Main View Context \(String(describing: self.taskOnMainContext?.notes?.notes))")
        }
        self.childContext?.performAndWait {
            print ("Task notes on Child View Context \(String(describing: self.taskOnChildContext?.notes?.notes)) \n")
        }
    }
    
    func saveChildContext() {
        self.childContext?.performAndWait {
            if let _childContext = self.childContext {
                do {
                    try CoreDataStack.sharedInstance.saveToStore(_childContext)
                } catch {
                    print("error saving child context \(error)")
                }
            }
        }
    }
    
    func discardChanges() {
        self.childContext?.performAndWait {
            //self.taskOnChildContext?.managedObjectContext?.reset()
            //reset() vs refresh()
            if let _taskOnChildContext = self.taskOnChildContext {
                self.childContext?.refresh(_taskOnChildContext, mergeChanges: false)
            }
        }
    }
    
}



