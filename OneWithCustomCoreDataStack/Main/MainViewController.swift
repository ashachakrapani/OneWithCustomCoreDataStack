//
//  ViewController.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 6/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import UIKit
import CoreData

//faults
//perform vs performAndWait
//connection pool - single write and multiple reads


class MainViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.seedData()
        self.tasks = self.getTasksFromCoreData()
        self.obtainPermanentObjectIds()
        self.randomController.getTaskOnRandomChildContext(task: self.tasks?.first)
        self.printTasks()
        self.editController.edit(task: self.tasks?.first)
        self.editController.printSelectedTaskOnAllContexts()
        self.editController.saveChildContext()
        //self.editController.discardChanges()
        self.editController.printSelectedTaskOnAllContexts()
        self.printTasks()
    }
    
    //MARK: Private API
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private lazy var coreDataController = CoreDataController()
    private lazy var editController = EditController()
    private lazy var randomController = SomeRandomController()
    private lazy var anotherRandomController = AnotherRandomController()
    private var selectedTaskMoc: TaskMOC?
    private var tasks: [TaskMOC]?
    
    private func seedData() {
        let _ = TaskMOC.insertInto(context: CoreDataStack.sharedInstance.mainViewContext, title: "Task 1", description: "Task 1 description", notes: nil)
        do {
            try CoreDataStack.sharedInstance.saveToStore(CoreDataStack.sharedInstance.mainViewContext)
        } catch {
            print ("Error saving seed data \(error)")
        }
    }
    
    private func getTasksFromCoreData() -> [TaskMOC]? {
        let tasks = coreDataController.getTasks(onContext: CoreDataStack.sharedInstance.mainViewContext)
        return tasks
    }
    
    private func printTasks() {
        if let _tasks = self.tasks {
            for task in _tasks {
                print("task on main context:  \(task)")
            }
        }
    }
    
    private func obtainPermanentObjectIds() {
        if let _tasks = self.tasks {
            do {
                try CoreDataStack.sharedInstance.mainViewContext.obtainPermanentIDs(for: _tasks)
            } catch {
                print("Error obtaining permanent object ids for MOCs \(error)")
            }
        }
    }
}





