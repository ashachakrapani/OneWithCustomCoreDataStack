//
//  CoreDataController.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 9/11/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

class CoreDataController {
    
    func getTasks(onContext context: NSManagedObjectContext) -> [TaskMOC]? {
        let fetchRequest: NSFetchRequest<TaskMOC> = TaskMOC.fetchRequest()
        var tasks: [TaskMOC]? = nil
        do {
            tasks = try context.fetch(fetchRequest)
        } catch {
            print("Error fetching task data from CoreData: \(error.localizedDescription)")
        }
        return tasks
    }
    
    func deleteTaskItem(_ taskMoc: TaskMOC, onContext context: NSManagedObjectContext) {
        
        do {
                context.delete(taskMoc)
                try CoreDataStack.sharedInstance.saveToStore(context)
        } catch {
            print(error.localizedDescription)
        }
    }
    
}
