//
//  CoreDataStack.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 8/24/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//


import Foundation
import CoreData

class CoreDataStack: NSObject {
    
    //MARK: - Singleton Implementation
    
//    private override init() {
//        super.init()
//    }
    
    static var sharedInstance = CoreDataStack()
    
    //MARK: - Private API
    
    private var dataModel = "CoreDataConcurrency"
    
    //The context that interacts with the PSC to save data to the store
    lazy var privateManagedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return managedObjectContext
    }()
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        guard let modelURL = Bundle.main.url(forResource: self.dataModel, withExtension: "momd") else {
            fatalError("Unable to find data model")
        }
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Unable to load data model")
        }
        return managedObjectModel
    }()
    
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let fileManager = FileManager.default
        let storeName = "\(self.dataModel).sqlite"
        
        let documentsDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let persistentStoreURL = documentsDirectoryURL.appendingPathComponent(storeName)
        print("Core data path : \(persistentStoreURL)" )
        do {
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                              configurationName: nil,
                                                              at: persistentStoreURL,
                                                              options: nil)
        } catch {
            fatalError("Unable to Load Persistent Store")
        }
        
        return persistentStoreCoordinator
    }()
    
    //MARK: - Public API
    lazy var mainViewContext: NSManagedObjectContext = {
        let mainViewContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        mainViewContext.parent = self.privateManagedObjectContext
        return mainViewContext
    }()
    
    ///Calling this save method saves all parent contexts until the data is saved all the way upto to the store
    func saveToStore(_ context: NSManagedObjectContext) throws {
        print("saving \(context)")
        try context.save()
        if let parentContext = context.parent {
            parentContext.performAndWait {
                do{
                    try self.saveToStore(parentContext)
                } catch {
                    print("Error saving parent context:\(parentContext) : \(error)")
                }
            }
        }
    }
    
}

