//
//  TaskMOC.swift
//  TestMergePolicy
//
//  Created by Asha Chakrapani on 6/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

@objc(TaskMOC)
public class TaskMOC: NSManagedObject {
    
    static func insertInto(context: NSManagedObjectContext, title: String, description: String, notes: String?) -> TaskMOC? {
        var objectMOC : TaskMOC? = nil
        if let entity = NSEntityDescription.entity(forEntityName: "Task", in: context) {
            objectMOC = TaskMOC(entity: entity, insertInto: context)
            objectMOC?.title = title
            objectMOC?.taskDescription = description
            objectMOC?.notes?.notes = notes
        }
        return objectMOC
    }
}
