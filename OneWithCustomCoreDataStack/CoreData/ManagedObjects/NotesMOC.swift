//
//  NotesMOC.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 9/11/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

@objc(NotesMOC)
public class NotesMOC: NSManagedObject {
    
    static func insertInto(context: NSManagedObjectContext, notes: String?) -> NotesMOC? {
        var objectMOC : NotesMOC? = nil
        if let entity = NSEntityDescription.entity(forEntityName: "Notes", in: context) {
            objectMOC = NotesMOC(entity: entity, insertInto: context)
            objectMOC?.notes = notes
        }
        return objectMOC
    }
}
