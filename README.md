# OneWithCustomCoreDataStack
This is a simple demo of how a Core Data Stack can be set up (one of the ways) to have multiple contexts. 

In this model, there is a private context that talks directly to the Store.
The main view context is a child context which is used across the app for all view updates.
Each view controller creates child contexts which is then used as a scratch pad for the changes being made on the corresponding UI which is either saved or discarded ( whenever the view controller is deallocated or by calling the refresh method programatically with merge changes as false)
